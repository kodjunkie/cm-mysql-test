<?php

use timgws\QueryBuilderParser;
use Illuminate\Database\Capsule\Manager as Capsule;

require_once(__DIR__ . '/bootstrap/app.php');

if (isset($_POST['query_rule']) && !empty($_POST['query_rule'])) {
    ?>

    <div class="col-md-6 offset-md-3 mt-4">

    <?php
        try {
            $table = Capsule::table('products');
            $qbp = new QueryBuilderParser(['identifier', 'name', 'price', 'category', 'in_stock']);
            $query = $qbp->parse(trim($_POST['query_rule']), $table);
            $sql = str_replace_array('?', $query->getBindings(), $query->toSql());
    ?>

            <div class="alert alert-success" role="alert">
                <h5 class="alert-heading">Query:</h5>
                <strong><?= $sql ?></strong>
            </div>
    <?php
        } catch (Exception $exception) {
            ?>
            <div class="alert alert-danger" role="alert">
                <h5 class="alert-heading">Error:</h5>
                <strong>An error occurred, please check your rule and try again.</strong>
            </div>
            <?php
        }
            ?>
    </div>
    <?php
}
