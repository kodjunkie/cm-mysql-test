##### II. Let's assume we have a MySQL table with the following definition (25pts):
Table : 
-   Products (identifier, name, price, category, in_stock)

Based on the Query builder Basic demo: [https://querybuilder.js.org/demo.html](https://querybuilder.js.org/demo.html)
Make a small tool to produce MySQL queries based on Query builder rules.
For example the following rule: 

```json
{
  "condition": "AND",
  "rules": [
    {
      "id": "price",
      "field": "price",
      "type": "double",
      "input": "number",
      "operator": "less",
      "value": 10.25
    },
    {
      "id": "name",
      "field": "name",
      "type": "string",
      "input": "text",
      "operator": "equal",
      "value": "Test"
    }
  ],
  "valid": true
}
```

Should produce the following mysql query: 
```sql
select * from `products` where `price` < 10.25 and `name` = Test
```

## Requirements
-   PHP >= 7.2.12
-   MySQL / MariaDB >= 5.7.16
-   Composer

## Installation
```bash
    git clone https://gitlab.com/Paplow/cm-mysql-test.git cm_mysql_test
    cd cm_mysql_test
    composer install
    # Update .env file accordingly
    composer run-script db:migrate 
```
After installation you can access it via ``http://{your-domain}``
