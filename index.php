<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Query Builder</title>
    <link rel="stylesheet" href="./public/assets/css/app.css">
</head>
<body>

<div class="container-fluid">

    <?php require './query_generator.php' ?>

    <div class="col-md-6 offset-md-3 mt-4">
        <form action="/" method="post">
            <div class="form-group">
                <label for="query_rule">Input Query Rule</label>
                <textarea class="form-control" name="query_rule" id="query_rule" rows="6" autofocus required></textarea>
            </div>

            <div>
                <button type="submit" class="btn btn-primary btn-outline-primary float-right">Generate Query</button>
            </div>
        </form>
    </div>
</div>

<script src="./public/assets/js/app.js"></script>
</body>
</html>
