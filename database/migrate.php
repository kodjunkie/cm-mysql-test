<?php

require_once __DIR__ . '/../bootstrap/app.php';

use Illuminate\Database\Capsule\Manager as Capsule;

// Drop all tables
Capsule::schema()->dropAllTables();

// Migrate all tables
require_once __DIR__ . '/migrations/create_products_table.php';

echo "Database successfully migrated. \n";
