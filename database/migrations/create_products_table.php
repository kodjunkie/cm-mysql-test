<?php

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('products', function ($table) {
    $table->increments('identifier');
    $table->string('name');
    $table->decimal('price', 12, 2);
    $table->string('category');
    $table->boolean('in_stock')->default(false);
});
